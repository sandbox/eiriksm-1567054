<?php

function restrict_by_roles_or_user_settings_form() {
  $form['restrict_by_roles_or_user_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message to replace content for restricted users'),
    '#default_value' => restrict_by_roles_or_user_var('message'),
    '#wysiyg' => FALSE,
  );
  $form['restrict_by_roles_or_user_message_anon'] = array(
    '#type' => 'textarea',
    '#title' => t('Additional message to display for restricted anonymous users'),
    '#default_value' => restrict_by_roles_or_user_var('message_anon'),
    '#wysiyg' => FALSE,
  );

  if (module_exists('token')) {
    $form['tokens'] = array(
      '#value' => theme('token_help', 'node'),
    );
  }

  return system_settings_form($form);
}
